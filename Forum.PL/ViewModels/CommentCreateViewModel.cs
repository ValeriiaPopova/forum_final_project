﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Forum.PL.ViewModels
{
    public class CommentCreateViewModel
    {
        [Required]
        [StringLength(10000)]
        public string Content { get; set; }

        [Required]
        public Guid ArticleId { get; set; }
    }
}