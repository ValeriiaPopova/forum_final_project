﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Forum.PL.ViewModels
{
    public class UserProfileUpdateViewModel
    {
        [StringLength(15, MinimumLength = 3)]
        public string UserName { get; set; }

        [EmailAddress]
        public string Email { get; set; }

        [StringLength(20)]
        public string Name { get; set; }
        [MinLength(8)]
        public string Password { get; set; }

    }
}
