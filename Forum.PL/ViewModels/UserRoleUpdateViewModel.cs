﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Forum.PL.ViewModels
{
    public class UserRoleUpdateViewModel
    {
        [Required]
        [RegularExpression("User|Moderator", ErrorMessage = "Role must be either User or Moderator")]
        public string Role { get; set; }
    }
}