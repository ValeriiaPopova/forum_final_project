﻿using System.ComponentModel.DataAnnotations;

namespace Forum.PL.ViewModels
{
    public class SingInViewModel
    {
        [Required]
        public string Login { get; set; }
        
        [Required]
        public string Password { get; set; }
    }
}