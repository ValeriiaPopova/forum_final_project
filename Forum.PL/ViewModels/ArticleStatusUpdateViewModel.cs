﻿using System.ComponentModel.DataAnnotations;

namespace Forum.PL.ViewModels
{
    public class ArticleStatusUpdateViewModel
    {
        [Required]
        public bool Closed { get; set; }
    }
}