﻿using System.ComponentModel.DataAnnotations;

namespace Forum.PL.ViewModels
{
    public class ArticleCreateViewModel
    {
        [Required]
        [StringLength(200, MinimumLength = 10)]
        public string Topic { get; set; }
    }
}