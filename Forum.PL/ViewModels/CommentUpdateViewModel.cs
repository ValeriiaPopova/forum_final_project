﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Forum.PL.ViewModels
{
    public class CommentUpdateViewModel
    {
        [Required]
        [StringLength(10000)]
        public string Content { get; set; }
    }
}