﻿using AutoMapper;
using Forum.BL.Models;
using Forum.PL.ViewModels;

namespace Forum.PL.AutoMapperViewModel
{
    public class AutoMapperModels : Profile
    {
        public AutoMapperModels()
        {
            CreateMap<ArticleCreateViewModel, ArticleCreateDTO>();
            CreateMap<ArticleStatusUpdateViewModel, ArticleStatusUpdateDTO>();
            CreateMap<ArticleUpdateViewModel, ArticleUpdateDTO>();
            CreateMap<CommentCreateViewModel, CommentCreateDTO>();
            CreateMap<CommentUpdateViewModel, CommentUpdateDTO>();
            CreateMap<SignUpViewModel, SignUpDTO>();
            CreateMap<SingInViewModel, SingInDTO>();
            CreateMap<UserProfileUpdateViewModel, UserProfileUpdateDTO>();
            CreateMap<UserRoleUpdateViewModel, UserRoleUpdateDTO>();
        }
    }
}
