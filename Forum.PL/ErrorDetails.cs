﻿namespace Forum.PL
{
    internal class ErrorDetails
    {
        public ErrorDetails()
        {
        }

        public int StatusCode { get; set; }
        public string MessageReceivedContext { get; set; }
    }
}