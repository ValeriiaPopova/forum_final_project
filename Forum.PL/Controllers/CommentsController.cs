﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Security.Claims;
using System.Threading.Tasks;
using Forum.BL.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Forum.BL.Interfaces;
using Forum.PL.ViewModels;
using AutoMapper;

namespace Forum.PL.Controllers
{
    /// <summary>
    /// comments controller
    /// </summary>
    [ApiController]
    [Route("api/comments")]
    public class CommentsController : ControllerBase
    {
        private readonly ICommentService _commentService;
        private readonly IMapper _mapper;

        /// <summary>
        /// Constructor for initializing a <see cref="CommentsController"/> class instance
        /// </summary>
        /// <param name="commentService">comment service</param>
        public CommentsController(ICommentService commentService, IMapper mapper)
        {
            _commentService = commentService;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets a specific comment by id
        /// </summary>
        /// <param name="id">Guid of the comment to be retrieved</param>
        /// <returns>The comment specified by <paramref name="id"/></returns>
        /// <response code="200">Returns the comment specified by <paramref name="id"/></response>
        /// <response code="404">comment with specified <paramref name="id"/> not found</response>
        [HttpGet("{id:guid}")]
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<CommentWithDetailsDTO>> GetById(Guid id)
        {
            return await _commentService.GetByIdAsync(id);
        }

        /// <summary>
        /// Gets all comments of the article specified by <paramref name="articleId"/>
        /// </summary>
        /// <param name="articleId">Guid of the article whose comments are to be retrieved</param>
        /// <returns>Array of comments related to the article</returns>
        /// <response code="200">Returns the array of comments</response>
        /// <response code="404">article with specified <paramref name="articleId"/> not found</response>
        [HttpGet]
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<IEnumerable<CommentWithDetailsDTO>>> GetByArticle(
            [FromQuery, Required] Guid articleId)
        {
            return Ok(await _commentService.GetByArticleAsync(articleId));
        }

        /// <summary>
        /// Creates new comment
        /// </summary>
        /// <param name="commentDTO">comment creation data</param>
        /// <returns>Newly created comment</returns>
        /// <response code="201">Returns the newly created comment</response>
        /// <response code="400">article specified by <paramref name="commentDTO.articleId"/> is closed for comments</response>
        /// <response code="404">article specified by <paramref name="commentDTO.articleId"/> not found</response>
        [HttpPost]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<CommentWithDetailsDTO>> Create(CommentCreateViewModel commentDTO)
        {
            var mapped = _mapper.Map<CommentCreateDTO>(commentDTO);
            var userId = new Guid(User.FindFirstValue(ClaimTypes.NameIdentifier));
            var result = await _commentService.CreateAsync(mapped, userId);
            return CreatedAtAction(nameof(GetById), new {id = result.Id}, result);
        }

        /// <summary>
        /// Updates the comment
        /// </summary>
        /// <param name="id">Guid of the comment to be updated</param>
        /// <param name="commentDTO">comment update data</param>
        /// <response code="204">comment has been updated</response>
        /// <response code="404">comment specified by <paramref name="id"/> not found</response>
        [HttpPut("{id:guid}")]
        [Authorize(Roles = "Moderator,Admin")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> Update(Guid id, CommentUpdateViewModel commentDTO)
        {
            var mapped = _mapper.Map<CommentUpdateDTO>(commentDTO);
            await _commentService.UpdateAsync(id, mapped);
            return NoContent();
        }

        /// <summary>
        /// Deletes the comment
        /// </summary>
        /// <param name="id">Guid of the comment to be deleted</param>
        /// <response code="204">comment has been deleted</response>
        /// <response code="404">comment specified by <paramref name="id"/> not found</response>
        [HttpDelete("{id:guid}")]
        [Authorize(Roles = "Moderator,Admin")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> Delete(Guid id)
        {
            await _commentService.DeleteAsync(id);
            return NoContent();
        }
    }
}