﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Forum.BL.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Forum.BL.Interfaces;
using Forum.PL.ViewModels;
using AutoMapper;

namespace Forum.PL.Controllers
{
    /// <summary>
    /// UsersProfile controller
    /// </summary>
    [ApiController]
    [Route("api/usersProfile")]
    public class UsersProfileController : ControllerBase
    {
        private readonly IUserProfileService _userProfileService;
        private readonly IMapper _mapper;

        /// <summary>
        /// Constructor for initializing a <see cref="UsersProfileController"/> class instance
        /// </summary>
        /// <param name="userProfileService">user Profile Service</param>
        public UsersProfileController(IUserProfileService userProfileService, IMapper mapper)
        {
            _userProfileService = userProfileService;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets a specific userProfile by id
        /// </summary>
        /// <param name="id">Guid of the userProfile to be retrieved</param>
        /// <returns>The userProfile specified by <paramref name="id"/></returns>
        /// <response code="200">Returns the userProfile specified by <paramref name="id"/></response>
        /// <response code="404">userProfile with specified <paramref name="id"/> not found</response>
        [HttpGet("{id:guid}")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<UserProfileDTO>> GetById(Guid id)
        {
            
            return await _userProfileService.GetByIdAsync(id);
        }

        /// <summary>
        /// Updates the userProfile
        /// </summary>
        /// <param name="id">Guid of the userProfile to be updated</param>
        /// <param name="userProfileDTO">userProfile update data</param>
        /// <response code="204">userProfile has been updated</response>
        /// <response code="404">userProfile specified by <paramref name="id"/> not found</response>
        [HttpPut("{id:guid}")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> Update(Guid id, UserProfileUpdateViewModel userProfileDTO)
        {
            var mapped = _mapper.Map<UserProfileUpdateDTO>(userProfileDTO);
            await _userProfileService.UpdateAsync(id, mapped);
            return NoContent();
        }
    }
}
