﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Forum.BL.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Forum.BL.Interfaces;
using Forum.PL.ViewModels;
using AutoMapper;

namespace Forum.PL.Controllers
{
    /// <summary>
    /// articles controller
    /// </summary>
    [ApiController]
    [Route("api/articles")]
    public class ArticlesController : ControllerBase
    {
        private readonly IArticleService _articleService;
        private readonly IMapper _mapper;

        /// <summary>
        /// Constructor for initializing a <see cref="ArticlesController"/> class instance
        /// </summary>
        /// <param name="articleService">article service</param>
        public ArticlesController(IArticleService articleService, IMapper mapper)
        {
            _articleService = articleService;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets a specific article by id
        /// </summary>
        /// <param name="id">Guid of the article to be retrieved</param>
        /// <returns>The article specified by <paramref name="id"/></returns>
        /// <response code="200">Returns the article specified by <paramref name="id"/></response>
        /// <response code="404">article with specified <paramref name="id"/> not found</response>
        [HttpGet("{id:guid}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [AllowAnonymous]
        public async Task<ActionResult<ArticleWithDetailsDTO>> GetById(Guid id)
        {
            return await _articleService.GetByIdAsync(id);
        }

        [AllowAnonymous]
        [HttpGet("categories/{category}")]
        public async Task<ActionResult<IEnumerable<ArticleWithDetailsDTO>>> GetArticleByCategory(ArticleCategoryEnum category, int skip, int? take)
        {
            return Ok(await _articleService.GetByCategoryAsync(category.ToString(), skip, take));
           
        }

        /// <summary>
        /// Gets all articles
        /// </summary>
        /// <returns>Array of articles</returns>
        /// <response code="200">Returns the array of articles</response>
        [HttpGet]
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<IEnumerable<ArticleWithDetailsDTO>>> GetAll()
        {
            return Ok(await _articleService.GetAllAsync());
        }

        /// <summary>
        /// Creates new article
        /// </summary>
        /// <param name="articleDTO">article creation data</param>
        /// <returns>Newly created article</returns>
        /// <response code="201">Returns the newly created article</response>
        [HttpPost]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status201Created)]
        public async Task<ActionResult<ArticleWithDetailsDTO>> Create(ArticleCreateViewModel articleDTO)
        {
            var mapped = _mapper.Map<ArticleCreateDTO>(articleDTO);
            var userId = new Guid(User.FindFirstValue(ClaimTypes.NameIdentifier));
            var result = await _articleService.CreateAsync(mapped, userId);
            return CreatedAtAction(nameof(GetById), new {id = result.Id}, result);
        }

        /// <summary>
        /// Updates the article
        /// </summary>
        /// <param name="id">Guid of the article to be updated</param>
        /// <param name="articleDTO">article update data</param>
        /// <response code="204">article has been updated</response>
        /// <response code="404">article specified by <paramref name="id"/> not found</response>
        [HttpPut("{id:guid}")]
        [Authorize(Roles = "Moderator,Admin")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> Update(Guid id, ArticleUpdateViewModel articleDTO)
        {
            var mapped = _mapper.Map<ArticleUpdateDTO>(articleDTO);
            await _articleService.UpdateAsync(id, mapped);
            return NoContent();
        }

        /// <summary>
        /// Deletes the article
        /// </summary>
        /// <param name="id">Guid of the article to be deleted</param>
        /// <response code="204">article has been deleted</response>
        /// <response code="404">article specified by <paramref name="id"/> not found</response>
        [HttpDelete("{id:guid}")]
        [Authorize(Roles = "Moderator,Admin")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> Delete(Guid id)
        {
            await _articleService.DeleteAsync(id);
            return NoContent();
        }

        /// <summary>
        /// Updates the article status
        /// </summary>
        /// <param name="id">Guid of the article whose status to be updated</param>
        /// <param name="statusDto">article status update data</param>
        /// <response code="204">article status has been updated</response>
        /// <response code="404">article specified by <paramref name="id"/> not found</response>
        [HttpPut("{id:guid}/status")]
        [Authorize(Roles = "Moderator,Admin")]
        public async Task<ActionResult> UpdateStatus(Guid id, [FromBody] ArticleStatusUpdateViewModel statusDto)
        {
            var mapped = _mapper.Map<ArticleStatusUpdateDTO>(statusDto);
            await _articleService.UpdateStatusAsync(id, mapped);
            return NoContent();
        }
    }
}