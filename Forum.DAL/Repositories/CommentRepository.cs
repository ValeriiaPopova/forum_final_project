﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Forum.DAL.Data;
using Forum.DAL.Data.Entities;
using Forum.DAL.Data.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Forum.DAL.Data.Repositories
{
    /// <inheritdoc cref="ICommentRepository" />
    public class CommentRepository : Repository<Comment>, ICommentRepository
    {
        /// <summary>
        /// Constructor for initializing a <see cref="CommentRepository"/> class instance
        /// </summary>
        /// <param name="context">Context of the database</param>
        public CommentRepository(ForumDbContext context) : base(context)
        {
        }

        public async Task<Comment> GetByIdWithDetailsAsync(Guid id)
        {
            return await Context.Comment.AsNoTracking()
                .Include(p => p.Author)
                .FirstOrDefaultAsync(p => p.Id == id);
        }

        public async Task<IEnumerable<Comment>> GetArticleCommentsWithDetailsAsync(Guid articleId)
        {
            return await Context.Comment.AsNoTracking()
                .Include(p => p.Author)
                .Where(p => p.ArticleId == articleId)
                .OrderBy(p => p.PublishDate)
                .ToListAsync();
        }
    }
}