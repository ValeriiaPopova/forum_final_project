﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Forum.DAL.Data;
using Forum.DAL.Data.Entities;
using Forum.DAL.Data.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Forum.DAL.Data.Repositories
{
    /// <inheritdoc cref="IArticleRepository" />
    public class ArticleRepository : Repository<Article>, IArticleRepository
    {
        /// <summary>
        /// Constructor for initializing a <see cref="ArticleRepository"/> class instance
        /// </summary>
        /// <param name="context">Context of the database</param>
        public ArticleRepository(ForumDbContext context) : base(context)
        {
        }

        public async Task<Article> GetByIdWithDetailsAsync(Guid id)
        {
            return await Context.Article.AsNoTracking()
                .Include(t => t.Author)
                .FirstOrDefaultAsync(t => t.Id == id);
        }

        public async Task<IEnumerable<Article>> GetAllWithDetailsAsync()
        {
            return await Context.Article
                .AsNoTracking()
                .Include(t => t.Author)
                .OrderByDescending(t => t.CreationDate)
                .ToListAsync();
        }
        public async Task<IEnumerable<Article>> GetByCategoryWithDetailsAsync(ArticleCategoryEnum name)
        {
            return await Context.Article.AsNoTracking()
                .Include(t => t.Author)
                .Where(c => c.ArticleCategory == name)
                .OrderByDescending(t => t.CreationDate)
                .ToListAsync();
        }
    }
}