﻿using Forum.DAL.Data;
using Forum.DAL.Data.Entities;
using Forum.DAL.Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Forum.DAL.Data.Repositories
{
    /// <inheritdoc />
    public class UserProfileRepository : IUserProfileRepository
    {
        private readonly ForumDbContext _context;


        /// <summary>
        /// Constructor for initializing a <see cref="UserProfileRepository"/> class instance
        /// </summary>
        /// <param name="context">Context of the database</param>
        public UserProfileRepository(ForumDbContext context)
        {
            _context = context;

        }

        public void Update(User entity)
        {
            _context.Entry(entity).State = EntityState.Modified;

        }
        public async Task<User> GetByIdAsync(Guid id)
        {
            return await _context.Users.FindAsync(id);
        }
    }
}
