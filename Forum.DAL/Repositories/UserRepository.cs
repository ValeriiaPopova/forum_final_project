﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Forum.DAL.Data;
using Forum.DAL.Data.Entities;
using Forum.DAL.Data.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Forum.DAL.Data.Repositories
{
    /// <inheritdoc />
    public class UserRepository : IUserRepository
    {
        private readonly ForumDbContext _context;
        /// <summary>
        /// Constructor for initializing a <see cref="UserRepository"/> class instance
        /// </summary>
        /// <param name="context">Context of the database</param>
        public UserRepository(ForumDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<User>> GetAllAsync()
        {
            return await _context.Users.AsNoTracking()
                .ToListAsync();
        }
    }
}