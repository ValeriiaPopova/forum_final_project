﻿using Forum.DAL.Data.Interfaces;
using Forum.DAL.Data.Repositories;
using System.Threading.Tasks;

namespace Forum.DAL.Data
{
    /// <inheritdoc />
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ForumDbContext _context;

        private ICommentRepository _commentRepository;
        private IArticleRepository _articleRepository;
        private IUserRepository _userRepository;
        private IUserProfileRepository _userProfileRepository;

        /// <summary>
        /// Constructor for initializing a <see cref="UnitOfWork"/> class instance
        /// </summary>
        /// <param name="context">Context of the database</param>
        public UnitOfWork(ForumDbContext context)
        {
            _context = context;
        }

        public ICommentRepository CommentRepository => _commentRepository ??= new CommentRepository(_context);
        public IArticleRepository ArticleRepository => _articleRepository ??= new ArticleRepository(_context);
        public IUserRepository UserRepository => _userRepository ??= new UserRepository(_context);
        public IUserProfileRepository UserProfileRepository => _userProfileRepository ??= new UserProfileRepository(_context);

        public async Task SaveAsync()
        {
            await _context.SaveChangesAsync();
        }
    }
}