﻿using System;
using System.Threading.Tasks;
using Forum.DAL.Configurations;
using Forum.DAL.Data.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace Forum.DAL.Data
{
    public class ForumDbContext : IdentityDbContext<User, IdentityRole<Guid>, Guid>
    {
        public ForumDbContext()
        {
        }

        public ForumDbContext(DbContextOptions<ForumDbContext> options) : base(options)
        {
        }
        
        public DbSet<Comment> Comment { get; set; }
        public DbSet<Article> Article { get; set; }
        
        protected override void OnModelCreating(ModelBuilder builder)
        {
            

            builder.ApplyConfiguration(new CommentConfiguration());
            builder.ApplyConfiguration(new ArticleConfiguration());
            builder.ApplyConfiguration(new UserConfiguration());


            base.OnModelCreating(builder);
        }
    }

    public class AppDbContextFactory : IDesignTimeDbContextFactory<ForumDbContext>
    {
        public ForumDbContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<ForumDbContext>();
            optionsBuilder.UseSqlServer("Server=WS210\\SQLEXPRESS;Database=Forum_Project;Trusted_Connection=True;");

            return new ForumDbContext(optionsBuilder.Options);
        }
    }
}