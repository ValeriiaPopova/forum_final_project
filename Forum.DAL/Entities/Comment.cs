﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Forum.DAL.Data.Entities
{
    public class Comment : EntityBase
    {
        [Required]
        [StringLength(10000)]
        public string Content { get; set; }

        public DateTime PublishDate { get; set; }
        
        public Guid ArticleId { get; set; }
        public Article Article { get; set; }
        
        public Guid? AuthorId { get; set; }
        public User Author { get; set; }
    }
}