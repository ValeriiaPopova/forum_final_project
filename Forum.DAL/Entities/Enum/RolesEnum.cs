﻿using System.Runtime.Serialization;

public enum RolesEnum
{
    [EnumMember(Value = "Admin")] Admin,
    [EnumMember(Value = "Moderator")] Moderator,
    [EnumMember(Value = "User")] User
}
