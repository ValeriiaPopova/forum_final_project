﻿using System.Runtime.Serialization;
/// <summary>
/// Enum for article categories
/// </summary>
public enum ArticleCategoryEnum
{
    [EnumMember(Value = "technology")] technology,
    [EnumMember(Value = "travels")] travels,
    [EnumMember(Value = "news")] news,
    [EnumMember(Value = "sport")] sport,
    [EnumMember(Value = "music")] music,
    [EnumMember(Value = "art")] art,
    [EnumMember(Value = "cooking")] cooking
    // [EnumMember(Value = "any")] any

}