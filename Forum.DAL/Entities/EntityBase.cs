﻿using System;

namespace Forum.DAL.Data.Entities
{
    public abstract class EntityBase
    {
        public Guid Id { get; set; }
    }
}