﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Forum.DAL.Data.Entities
{
    public class Article : EntityBase
    {
        [Required]
        [StringLength(200, MinimumLength = 10)]
        public string Topic { get; set; }
        public ArticleCategoryEnum ArticleCategory { get; set; }

        public bool Closed { get; set; }
        
        public DateTime CreationDate { get; set; }

        public IEnumerable<Comment> Comment { get; set; }

        public Guid? AuthorId { get; set; }
        public User Author { get; set; }
    }
}