﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;

namespace  Forum.DAL.Data.Entities
{
    public class User : IdentityUser<Guid>
    {
        [StringLength(20)]
        public string Name { get; set; }

        public DateTime RegistrationDate { get; set; }

        public IEnumerable<Comment> Comment { get; set; }
        public IEnumerable<Article> Article { get; set; }
    }
}