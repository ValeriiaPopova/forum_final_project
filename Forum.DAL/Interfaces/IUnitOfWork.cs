﻿using System.Threading.Tasks;

namespace Forum.DAL.Data.Interfaces
{
    /// <summary>
    /// Unit of work
    /// </summary>
    public interface IUnitOfWork
    {
        public ICommentRepository CommentRepository { get; }
        public IArticleRepository ArticleRepository { get; }
        public IUserRepository UserRepository { get; }
        public IUserProfileRepository UserProfileRepository { get; }
        
        /// <summary>
        /// Saves all changes made through the repositories in the context to the database
        /// </summary>
        public Task SaveAsync();
    }
}