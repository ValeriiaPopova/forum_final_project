﻿using Forum.DAL.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Forum.DAL.Data.Interfaces
{
    /// <summary>
    /// Repository of <see cref="User"/> entities
    /// </summary>
    public interface IUserProfileRepository
    {
        /// <summary>
        /// Updates userProfile
        /// </summary>
        /// <param name="entity">userProfile to be started tracking by context as modified</param>
        void Update(User entity);
        /// <summary>
        /// Retrieves userProfile specified by <paramref name="id"/>
        /// </summary>
        /// <param name="id">Id of the userProfile to be retrieved</param>
        /// <returns>
        /// Entity specified by <paramref name="id"/>, if exists, <c>null</c> otherwise
        /// </returns>
        Task<User> GetByIdAsync(Guid id);
    }
}
