﻿using Forum.DAL.Data.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Forum.DAL.Data.Interfaces
{
    /// <summary>
    /// Repository of <see cref="Comment"/> entities
    /// </summary>
    public interface ICommentRepository : IRepository<Comment>
    {
        /// <summary>
        /// Retrieves comment specified by <paramref name="id"/>
        /// </summary>
        /// <param name="id">Guid of the comment to be retrieved</param>
        /// <returns>
        /// <see cref="Comment"/> including its author, if exists, <c>null</c> otherwise/>
        /// </returns>
        public Task<Comment> GetByIdWithDetailsAsync(Guid id);

        /// <summary>
        /// Retrieves all commentS related to the article specified by <paramref name="articleId"/>
        /// </summary>
        /// <param name="articleId">Guid of the article whose comments to be retrieved</param>
        /// <returns>List of comments, including their authors, ordered by publish date by ascending order</returns>
        public Task<IEnumerable<Comment>> GetArticleCommentsWithDetailsAsync(Guid articleId);
    }
}