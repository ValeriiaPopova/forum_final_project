﻿using Forum.DAL.Data.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Forum.DAL.Data.Interfaces
{
    /// <summary>
    /// Repository of <see cref="Article"/> entities
    /// </summary>
    public interface IArticleRepository : IRepository<Article>
    {
        /// <summary>
        /// Retrieves article specified by <paramref name="id"/>
        /// </summary>
        /// <param name="id">Guid of the article to be retrieved</param>
        /// <returns>
        /// <see cref="Article"/> including its author, if exists, <c>null</c> otherwise/>
        /// </returns>
        public Task<Article> GetByIdWithDetailsAsync(Guid id);

        /// <summary>
        /// Retrieves all articles
        /// </summary>
        /// <returns>
        /// List of articles, including their authors, ordered by creation date by descending order
        /// </returns>
        public Task<IEnumerable<Article>> GetAllWithDetailsAsync();
        /// <summary>
        /// Gets a specific post by category
        /// </summary>
        /// <param name="name">ArticleCategoryEnum of the post to be retrieved</param>
        /// <returns>The post/></returns>
        /// <exception cref="NotFoundException">
        /// Thrown when the post with specified <paramref name="name"/> does not exist
        /// </exception>
        Task<IEnumerable<Article>> GetByCategoryWithDetailsAsync(ArticleCategoryEnum name);
    }
}