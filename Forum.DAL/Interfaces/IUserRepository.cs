﻿using Forum.DAL.Data.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Forum.DAL.Data.Interfaces
{
    /// <summary>
    /// Repository of <see cref="User"/> entities
    /// </summary>
    public interface IUserRepository
    {
        /// <summary>
        /// Retrieves all users
        /// </summary>
        /// <returns>
        /// List of application users
        /// </returns>
        public Task<IEnumerable<User>> GetAllAsync();

    }
}