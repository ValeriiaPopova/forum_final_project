﻿using System;
using Forum.DAL.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Forum.DAL.Configurations
{
    public class ArticleConfiguration : IEntityTypeConfiguration<Article>
    {
        public void Configure(EntityTypeBuilder<Article> builder)
        {
            builder.HasKey(t => t.Id);

            builder.HasOne(t => t.Author)
                .WithMany(u => u.Article)
                .HasForeignKey(t => t.AuthorId)
                .OnDelete(DeleteBehavior.SetNull);

            builder.Property(t => t.Closed)
                .HasDefaultValue(false);
            
            builder.Property(p => p.CreationDate)
                .HasConversion(d => d,d => DateTime.SpecifyKind(d, DateTimeKind.Utc))
                .HasDefaultValueSql("GETUTCDATE()");
        }
    }
}