﻿using System;
using Forum.DAL.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Forum.DAL.Configurations
{
    public class CommentConfiguration : IEntityTypeConfiguration<Comment>
    {
        public void Configure(EntityTypeBuilder<Comment> builder)
        {
            builder.HasKey(p => p.Id);
            
            builder.HasOne(p => p.Article)
                .WithMany(t => t.Comment)
                .HasForeignKey(t => t.ArticleId)
                .OnDelete(DeleteBehavior.Cascade);
            
            builder.HasOne(p => p.Author)
                .WithMany(u => u.Comment)
                .HasForeignKey(p => p.AuthorId)
                .OnDelete(DeleteBehavior.SetNull);

            builder.Property(p => p.PublishDate)
                .HasConversion(d => d,d => DateTime.SpecifyKind(d, DateTimeKind.Utc))
                .HasDefaultValueSql("GETUTCDATE()");
        }
    }
}