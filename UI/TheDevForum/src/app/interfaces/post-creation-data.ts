export interface PostCreationData {
  content: string,
  articleId: string
}
