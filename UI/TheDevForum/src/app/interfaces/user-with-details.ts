export interface UserWithDetails {
  id: string,
  username: string,
  email: string,
  name: string,
  registrationDate: Date,
  roles: string[]
}
