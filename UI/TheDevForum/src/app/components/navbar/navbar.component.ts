import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {AuthService} from "../../services/auth.service";
import { Session } from "../../interfaces/session";
import { User } from 'src/app/interfaces/user';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class NavbarComponent implements OnInit {

  constructor(private auth: AuthService,
              private route: ActivatedRoute) {
  }

  loading = true;
  id = this.auth.session?.userId;
  // session!: Session;
  // currentUser!: User;
    // id!: string | null;

  ngOnInit(): void {
  //   this.route.paramMap.subscribe(params => { 
  //     this.id = params.get('id'); 
  //     // this.router.navigate(['/userProfile/', this.id]);
  //  });
  }

  get isLoggedIn(): boolean {
    return this.auth.isLoggedIn;
  }

  get userName(): string {
    return this.auth.session?.username!;
  }

  get userId(): string {
    return this.auth.session?.userId!;
  }

  get isAdmin(): boolean | undefined {
    return this.auth.session?.userRoles.includes("Admin");
  }

  get isUser(): boolean | undefined {
    return this.auth.session?.userRoles.includes("User");
  }

  signOut() {
    this.auth.signOut();
  }
}
