import {Component, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {AuthService} from "../../services/auth.service";
import {MatDialog} from "@angular/material/dialog";
import {NewThreadDialogComponent} from "./new-thread-dialog/new-thread-dialog.component";
import {ThreadWithDetails} from "../../interfaces/thread-with-details";
import {ThreadCreationData} from "../../interfaces/thread-creation-data";
import {NotificationService} from "../../services/notification.service";
import {ActivatedRoute, Router} from "@angular/router";
import {ThreadStatusUpdateData} from "../../interfaces/thread-status-update-data";
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-threads',
  templateUrl: './threads.component.html',
  styleUrls: ['./threads.component.scss']
})
export class ThreadsComponent implements OnInit {

  loading = true;
  threads!: ThreadWithDetails[];

  constructor(private api: HttpClient,
              private auth: AuthService,
              private dialog: MatDialog,
              private ns: NotificationService,
              private router: Router,
              private route: ActivatedRoute) {
  }

  apiBaseUrl = environment.apiUrlPrefix;

  ngOnInit(): void {
    this.api.get<ThreadWithDetails[]>(this.apiBaseUrl + '/api/articles')
      .subscribe({
        next: threads => {
          this.threads = threads;
          this.loading = false;
        },
        error: err => {
          this.ns.notifyError(`Loading data failed. Error ${err.status}`, true);
        }
      })
  }

  get isLoggedIn(): boolean {
    return this.auth.isLoggedIn;
  }

  get isUserModeratorOrAdmin(): boolean | undefined {
    if (!this.auth.isLoggedIn) {
      return;
    }

    const roles = this.auth.session?.userRoles;

    return roles?.includes('Moderator') ||
      roles?.includes('Admin');
  }

  openNewThreadDialog() {
    const dialogRef = this.dialog.open(NewThreadDialogComponent,
      {
        maxWidth: '600px',
        width: '100%'
      });

    dialogRef.afterClosed().subscribe(result => {
        if (result !== undefined) {
          this.createNewThread(result);
        }
      }
    );
  }

  createNewThread(topic: string) {
    topic = topic.trim();

    this.api.post<ThreadWithDetails>(this.apiBaseUrl + '/api/articles', <ThreadCreationData>{topic})
      .subscribe({
        next: thread => {
          this.threads.unshift(thread);
          this.ns.notifySuccess("New article has been successfully created");
          this.router.navigate([thread.id], {relativeTo: this.route});
        },
        error: err => {
          this.ns.notifyError(`Failed to create a article. Error ${err.status}`);
        }
      });
  }

  updateThreadStatus(thread: ThreadWithDetails, closed: boolean) {
    this.api.put<ThreadStatusUpdateData>(this.apiBaseUrl + '/api/articles/'+ thread.id + '/status', {closed})
      .subscribe(
        {
          next: () => {
            thread.closed = closed;
            this.ns.notifySuccess(`Article has been ${closed ? 'closed' : 'opened'}`);
          },
          error: err => {
            this.ns.notifyError(`Operation failed. Error ${err.status}`);
          }
        }
      );
  }

  deleteThread(thread: ThreadWithDetails) {
    this.api.delete(this.apiBaseUrl + '/api/articles/'+ thread.id , undefined)
      .subscribe(
        {
          next: () => {
            const index = this.threads.indexOf(thread);
            if (index !== -1) {
              this.threads.splice(index, 1);
            }

            this.ns.notifySuccess("Article has been deleted");
          },
          error: err => {
            this.ns.notifyError(`Article deletion failed. Error ${err.status}`);
          }
        }
      );
  }

  // getArticleByCategory(articleCategory: string): void {
  //   this.api.get<ThreadWithDetails[]>(this.apiBaseUrl + '/api/articles/categories/' + articleCategory)
  //   .subscribe({
  //     next: threadsByCategory=> {
  //       this.threads = threadsByCategory
  //       this.filter=articleCategory;
  //       // this.filter = articleCategory;
  //       this.loading = false;
  //     },
  //     error: err => {
  //       this.ns.notifyError(`Loading data failed. Error ${err.status}`, true);
  //     }
  //   })
  // }
}
