import { HttpClient, HttpStatusCode } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { UserWithDetails } from 'src/app/interfaces/user-with-details';
import { UsersProfileUpdateData } from 'src/app/interfaces/users-profile-update-data';
import { UsersProfile } from 'src/app/interfaces/usersProfile';
import { AuthService } from 'src/app/services/auth.service';
import { NotificationService } from 'src/app/services/notification.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-users-profile',
  templateUrl: './users-profile.component.html',
  styleUrls: ['./users-profile.component.scss']
})

export class UsersProfileComponent implements OnInit {

   loading = true;
   hidePassword = true;
  // displayedColumns = ['username', 'email', 'name', 'registrationDate', 'roles', 'options'];
  // dataSource = new MatTableDataSource<UserWithDetails>();
  // filterValue = '';

  // @ViewChild(MatPaginator) paginator!: MatPaginator;
  // @ViewChild(MatSort) sort!: MatSort;

  constructor(private api: HttpClient,
              private ns: NotificationService,
              private auth: AuthService,
              private route: ActivatedRoute,
              private router: Router,
              // private name: string,
              // private email: string,
              // private username: string,
              // private password: string
              ) {
  }

  apiBaseUrl = environment.apiUrlPrefix;
  userProfile!: UsersProfileUpdateData;

  ngOnInit(): void {

            this.api.get<UsersProfile>(this.apiBaseUrl + '/api/usersProfile/' + this.auth.session?.userId).subscribe(
              response=>{
                  this.userProfile = response;
              }
            );

  }

  onSubmit(): void{
    const updateRequest: UsersProfileUpdateData = {
      name: this.userProfile?.name,
      email: this.userProfile?.email,
      userName: this.userProfile?.userName,
      password: this.userProfile?.password
    }
    
      this.api.put(this.apiBaseUrl + '/api/usersProfile/' + this.auth.session?.userId, updateRequest)
        .subscribe({
              next: () => {
               
                this.ns.notifySuccess(`Data has been updated`);
              },
              error: err => {
              //  if(updateRequest.email == null || updateRequest.name==null || updateRequest.userName==null)
               //// {
                  this.ns.notifyError(`Invalid value (email, name and user name should be not null) Length name and user name should be more 3 and not char 40. Password should be min 8 char.`);
                //} 
                // this.ns.notifyError(`Operation failed. Error ${err.status}`);
              }
            });
  }


get isLoggedIn(): boolean {
  return this.auth.isLoggedIn;
}

}
