import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {SignInComponent} from "./components/sign-in/sign-in.component";
import {SignUpComponent} from "./components/sign-up/sign-up.component";
import {ThreadsComponent} from "./components/threads/threads.component";
import {PostsComponent} from "./components/posts/posts.component";
import {UsersComponent} from "./components/users/users.component";
import {OnlyAdminsGuard} from "./guards/only-admins-guard.service";
import {OnlyGuestsGuard} from "./guards/only-guests-guard.service";
import { UsersProfileComponent } from './components/users-profile/users-profile.component';
const routes: Routes = [
  {path: 'articles', component: ThreadsComponent},
  {path: 'articles/:id', component: PostsComponent},
  {path: 'sign-in', component: SignInComponent, canActivate: [OnlyGuestsGuard]},
  {path: 'sign-up', component: SignUpComponent, canActivate: [OnlyGuestsGuard]},
  {path: 'users', component: UsersComponent, canActivate: [OnlyAdminsGuard]},
  // {path: 'usersProfile', component: UsersProfileComponent},
  {path: 'usersProfile/:id', component: UsersProfileComponent},
  {path: '**', redirectTo: 'articles'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
