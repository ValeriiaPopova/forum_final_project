﻿using Forum.DAL.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace Forum.BL.Unit
{
    public class UnitWork : UnitOfWork, IUnitWork
    {
        public UnitWork(ForumDbContext context) : base(context)
        {
        }
    }
}
