﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Forum.BL.Exceptions
{
    /// <summary>
    /// The exception that is thrown when bad request
    /// </summary>
    [Serializable]
    public class BadRequestException : ForumException
    {
        public BadRequestException()
        {
        }

        protected BadRequestException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public BadRequestException(string message) : base(message)
        {
        }

        public BadRequestException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
