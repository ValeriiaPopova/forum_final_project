﻿using System.ComponentModel.DataAnnotations;

namespace Forum.BL.Models
{
    public class UserProfileDTO
    {
        public string UserName { get; set; }
        
        public string Email { get; set; }
        
        public string Name { get; set; }
    }
}