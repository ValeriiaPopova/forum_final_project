﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Forum.BL.Models
{
    public class UserRoleUpdateDTO
    {
        public string Role { get; set; }
    }
}