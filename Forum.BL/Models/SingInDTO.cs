﻿using System.ComponentModel.DataAnnotations;

namespace Forum.BL.Models
{
    public class SingInDTO
    {
        public string Login { get; set; }
        
        public string Password { get; set; }
    }
}