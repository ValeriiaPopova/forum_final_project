﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Forum.BL.Models
{
    public class CommentUpdateDTO
    {
        public string Content { get; set; }
    }
}