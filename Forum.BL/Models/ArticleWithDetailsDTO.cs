﻿using System;

namespace Forum.BL.Models
{
    public class ArticleWithDetailsDTO
    {
        public Guid Id { get; set; }
        public string Topic { get; set; }
        public bool Closed { get; set; }
        public DateTime CreationDate { get; set; }
        public UserDTO Author { get; set; }
        public string ArticleCategory { get; set; }
    }
}