﻿using System.ComponentModel.DataAnnotations;

namespace Forum.BL.Models
{
    public class ArticleStatusUpdateDTO
    {
        public bool Closed { get; set; }
    }
}