﻿using System.ComponentModel.DataAnnotations;

namespace Forum.BL.Models
{
    public class SignUpDTO
    {
        public string UserName { get; set; }
        
        public string Email { get; set; }
        
        public string Name { get; set; }
        public string Password { get; set; }
    }
}