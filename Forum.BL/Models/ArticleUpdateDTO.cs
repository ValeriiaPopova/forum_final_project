﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Forum.BL.Models
{
    public class ArticleUpdateDTO
    {
        public string Topic { get; set; }
    }
}