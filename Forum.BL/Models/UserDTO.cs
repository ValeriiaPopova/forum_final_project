﻿using System;

namespace Forum.BL.Models
{
    public class UserDTO
    {
        public Guid Id { get; set; }
        public string Username { get; set; }
        public string Name { get; set; }
        public DateTime RegistrationDate { get; set; }
    }
}