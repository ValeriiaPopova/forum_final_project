﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Forum.BL.Models
{
    public class CommentCreateDTO
    {
        public string Content { get; set; }

        public Guid ArticleId { get; set; }
    }
}