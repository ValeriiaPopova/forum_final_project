﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Forum.BL.Models
{
    public class UserProfileUpdateDTO
    {
        public string UserName { get; set; }

        public string Email { get; set; }

        public string Name { get; set; }
        public string Password { get; set; }

    }
}
