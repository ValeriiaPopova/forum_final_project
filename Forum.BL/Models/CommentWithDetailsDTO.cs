﻿using System;

namespace Forum.BL.Models
{
    public class CommentWithDetailsDTO
    {
        public Guid Id { get; set; }
        public string Content { get; set; }
        public DateTime PublishDate { get; set; }
        public Guid ArticleId { get; set; }
        public UserDTO Author { get; set; }
    }
}