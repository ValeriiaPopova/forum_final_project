﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Forum.BL.Models;
using Forum.DAL.Data.Entities;
using Forum.DAL.Data.Interfaces;
using Microsoft.AspNetCore.Identity;
using Forum.BL.Exceptions;
using Forum.BL.Interfaces;

namespace Forum.BL.Services
{
    /// <inheritdoc />
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly UserManager<User> _userManager;
        private readonly IMapper _mapper;

        /// <summary>
        /// Constructor for initializing a <see cref="UserService"/> class instance
        /// </summary>
        /// <param name="unitOfWork">Unit of work</param>
        /// <param name="userManager">Identity user manager</param>
        /// <param name="mapper">Mapper</param>
        public UserService(IUnitOfWork unitOfWork, UserManager<User> userManager, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _userManager = userManager;
            _mapper = mapper;
        }

        public async Task<IEnumerable<UserWithDetailsDTO>> GetAllAsync()
        {
            var users = await _unitOfWork.UserRepository.GetAllAsync();
            var result = new List<UserWithDetailsDTO>();

            foreach (var user in users)
            {
                var userDto = _mapper.Map<UserWithDetailsDTO>(user);
                userDto.Roles = await _userManager.GetRolesAsync(user);
                result.Add(userDto);
            }

            return result;
        }

        public async Task UpdateRoleAsync(Guid id, UserRoleUpdateDTO roleDto)
        {
            var user = await _userManager.FindByIdAsync(id.ToString());
            if (user == null)
            {
                throw new NotFoundException();
            }

            var userRoles = await _userManager.GetRolesAsync(user);

            if (userRoles.Contains("Admin", StringComparer.OrdinalIgnoreCase))
            {
                throw new ForumException("Impossible to change role of admin");
            }

            if (userRoles.Contains(roleDto.Role, StringComparer.OrdinalIgnoreCase))
            {
                throw new ForumException($"User '{user.UserName}' already in role '{roleDto.Role}'");
            }

            await _userManager.RemoveFromRolesAsync(user, userRoles);
            await _userManager.AddToRoleAsync(user, roleDto.Role);
        }

        public async Task DeleteAsync(Guid id)
        {
            var user = await _userManager.FindByIdAsync(id.ToString());
            if (user == null)
            {
                throw new NotFoundException();
            }

            if (await _userManager.IsInRoleAsync(user, "Admin"))
            {
                throw new ForumException("Unable to delete admin");
            }

            await _userManager.DeleteAsync(user);
        }


    }
}