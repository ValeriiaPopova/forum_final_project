﻿using AutoMapper;
using Forum.BL.Models;
using Forum.DAL.Data.Entities;
using Forum.DAL.Data.Interfaces;
using Microsoft.AspNetCore.Identity;
using Forum.BL.Exceptions;
using Forum.BL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Forum.BL.Services
{
    /// <inheritdoc />
    public class UserProfileService : IUserProfileService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly UserManager<User> _userManager;
        private readonly IMapper _mapper;

        /// <summary>
        /// Constructor for initializing a <see cref="UserProfileService"/> class instance
        /// </summary>
        /// <param name="unitOfWork">Unit of work</param>
        /// <param name="userManager">Identity user manager</param>
        /// <param name="mapper">Mapper</param>
        public UserProfileService(IUnitOfWork unitOfWork, UserManager<User> userManager, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _userManager = userManager;
            _mapper = mapper;
        }
        public async Task UpdateAsync(Guid id, UserProfileUpdateDTO userProfileDTO)
        {
            var user = _userManager.FindByIdAsync(id.ToString());
            if (user == null)
            {
                throw new NotFoundException();
            }
            // Update it with the values from the view model
            user.Result.UserName = userProfileDTO.UserName;
            user.Result.NormalizedUserName = userProfileDTO.UserName.ToUpper();
            user.Result.Email = userProfileDTO.Email;
            user.Result.NormalizedEmail = userProfileDTO.Email.ToUpper();
            user.Result.Name = userProfileDTO.Name;
            if (userProfileDTO.Password != null)
            {
                await _userManager.ChangePasswordAsync(user.Result, user.Result.PasswordHash, userProfileDTO.Password);
            }
  
              
        //    var mapped = _mapper.Map(userProfileDTO, userProfileDTO);
            // Apply the changes if any to the db
            await _userManager.UpdateAsync(user.Result);
          //  var mapped = _mapper.Map(user, userProfileDTO);

            await _unitOfWork.SaveAsync();
        }

        public async Task<UserProfileDTO> GetByIdAsync(Guid id)
        {
            var user = await _unitOfWork.UserProfileRepository.GetByIdAsync(id);
            if (user == null)
            {
                throw new NotFoundException();
            }

            return _mapper.Map<UserProfileDTO>(user);
        }
    }
}
