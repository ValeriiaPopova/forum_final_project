﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Forum.BL.Interfaces;
using Forum.BL.Models;
using Forum.DAL.Data.Entities;
using Forum.DAL.Data.Interfaces;
using Microsoft.AspNetCore.Identity;
using Forum.BL.Exceptions;
using System.Linq;

namespace Forum.BL.Services
{
    /// <inheritdoc />
    public class ArticleService : IArticleService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly UserManager<User> _userManager;
        private readonly IMapper _mapper;

        /// <summary>
        /// Constructor for initializing a <see cref="ArticleService"/> class instance
        /// </summary>
        /// <param name="unitOfWork">Unit of work</param>
        /// <param name="userManager">Identity user manager</param>
        /// <param name="mapper">Mapper</param>
        public ArticleService(IUnitOfWork unitOfWork, UserManager<User> userManager, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _userManager = userManager;
            _mapper = mapper;
        }

        public async Task<ArticleWithDetailsDTO> GetByIdAsync(Guid id)
        {
            var article = await _unitOfWork.ArticleRepository.GetByIdWithDetailsAsync(id);
            if (article == null)
            {
                throw new NotFoundException();
            }

            return _mapper.Map<ArticleWithDetailsDTO>(article);
        }

        public async Task<IEnumerable<ArticleWithDetailsDTO>> GetAllAsync()
        {
            var article = await _unitOfWork.ArticleRepository.GetAllWithDetailsAsync();
            return _mapper.Map<IEnumerable<ArticleWithDetailsDTO>>(article);
        }

        public async Task<ArticleWithDetailsDTO> CreateAsync(ArticleCreateDTO articleDTO, Guid authorId)
        {
            var user = await _userManager.FindByIdAsync(authorId.ToString());
            if (user == null)
            {
                throw new ForumException($"User with id '{authorId}' does not exist");
            }

            var article = _mapper.Map<Article>(articleDTO);
            article.Author = user;

            _unitOfWork.ArticleRepository.Add(article);
            await _unitOfWork.SaveAsync();

            return _mapper.Map<ArticleWithDetailsDTO>(article);
        }

        public async Task UpdateAsync(Guid id, ArticleUpdateDTO articleDTO)
        {
            var article = await _unitOfWork.ArticleRepository.GetByIdAsync(id);
            if (article == null)
            {
                throw new NotFoundException();
            }

            _mapper.Map(articleDTO, article);

            _unitOfWork.ArticleRepository.Update(article);
            await _unitOfWork.SaveAsync();
        }

        public async Task DeleteAsync(Guid id)
        {
            var article = await _unitOfWork.ArticleRepository.GetByIdAsync(id);
            if (article == null)
            {
                throw new NotFoundException();
            }

            _unitOfWork.ArticleRepository.Delete(article);
            await _unitOfWork.SaveAsync();
        }

        public async Task UpdateStatusAsync(Guid id, ArticleStatusUpdateDTO statusDTO)
        {
            var article = await _unitOfWork.ArticleRepository.GetByIdAsync(id);
            if (article == null)
            {
                throw new NotFoundException();
            }

            _mapper.Map(statusDTO, article);

            _unitOfWork.ArticleRepository.Update(article);
            await _unitOfWork.SaveAsync();
        }

        public async Task<IEnumerable<ArticleWithDetailsDTO>> GetByCategoryAsync(string category, int skip, int? take)
        {
            IEnumerable<Article> unmapped;
            if (category == null)
            {
                throw new BadRequestException("Invalid category");
            }
            else if (category == ArticleCategoryEnum.art.ToString())
            {
                unmapped = await _unitOfWork.ArticleRepository.GetByCategoryWithDetailsAsync(ArticleCategoryEnum.art);

            }
            else if (category == ArticleCategoryEnum.cooking.ToString())
            {
                unmapped = await _unitOfWork.ArticleRepository.GetByCategoryWithDetailsAsync(ArticleCategoryEnum.cooking);

            }
            else if (category == ArticleCategoryEnum.music.ToString())
            {
                unmapped = await _unitOfWork.ArticleRepository.GetByCategoryWithDetailsAsync(ArticleCategoryEnum.music);

            }
            else if (category == ArticleCategoryEnum.news.ToString())
            {
                unmapped = await _unitOfWork.ArticleRepository.GetByCategoryWithDetailsAsync(ArticleCategoryEnum.news);

            }
            else if (category == ArticleCategoryEnum.sport.ToString())
            {
                unmapped = await _unitOfWork.ArticleRepository.GetByCategoryWithDetailsAsync(ArticleCategoryEnum.sport);

            }
            else if (category == ArticleCategoryEnum.technology.ToString())
            {
                unmapped = await _unitOfWork.ArticleRepository.GetByCategoryWithDetailsAsync(ArticleCategoryEnum.technology);

            }
            else if (category == ArticleCategoryEnum.travels.ToString())
            {
                unmapped = await _unitOfWork.ArticleRepository.GetByCategoryWithDetailsAsync(ArticleCategoryEnum.travels);

            }
            else
            {
                unmapped = await _unitOfWork.ArticleRepository.GetAllAsync();

            }
            if (unmapped.Count() == 0)
            {
                throw new NotFoundException("Not found article by this article");

            }
            else
            {
                if (take != null)
                {
                    return _mapper.Map<IEnumerable<ArticleWithDetailsDTO>>(unmapped.Skip(skip).Take((int)take));
                }
                else
                {
                    return _mapper.Map<IEnumerable<ArticleWithDetailsDTO>>(unmapped.Skip(skip));

                }
            }
        }
    }
}