﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Forum.BL.Models;
using Forum.DAL.Data.Entities;
using Forum.DAL.Data.Interfaces;
using Microsoft.AspNetCore.Identity;
using Forum.BL.Exceptions;
using Forum.BL.Interfaces;

namespace Forum.BL.Services
{
    /// <inheritdoc />
    public class CommentService : ICommentService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly UserManager<User> _userManager;
        private readonly IMapper _mapper;

        /// <summary>
        /// Constructor for initializing a <see cref="CommentService"/> class instance
        /// </summary>
        /// <param name="unitOfWork">Unit of work</param>
        /// <param name="userManager">Identity user manager</param>
        /// <param name="mapper">Mapper</param>
        public CommentService(IUnitOfWork unitOfWork, UserManager<User> userManager, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _userManager = userManager;
            _mapper = mapper;
        }

        public async Task<CommentWithDetailsDTO> GetByIdAsync(Guid id)
        {
            var comment = await _unitOfWork.CommentRepository.GetByIdWithDetailsAsync(id);
            if (comment == null)
            {
                throw new NotFoundException();
            }

            return _mapper.Map<CommentWithDetailsDTO>(comment);
        }

        public async Task<IEnumerable<CommentWithDetailsDTO>> GetByArticleAsync(Guid articleId)
        {
            var article = await _unitOfWork.ArticleRepository.GetByIdAsync(articleId);
            if (article == null)
            {
                throw new NotFoundException();
            }

            var comments = await _unitOfWork.CommentRepository.GetArticleCommentsWithDetailsAsync(articleId);
            return _mapper.Map<IEnumerable<CommentWithDetailsDTO>>(comments);
        }

        public async Task<CommentWithDetailsDTO> CreateAsync(CommentCreateDTO commentDTO, Guid authorId)
        {
            var article = await _unitOfWork.ArticleRepository.GetByIdAsync(commentDTO.ArticleId);
            if (article == null)
            {
                throw new NotFoundException();
            }

            var user = await _userManager.FindByIdAsync(authorId.ToString());
            if (user == null)
            {
                throw new ForumException($"User with id '{authorId}' does not exist");
            }

            if (article.Closed)
            {
                throw new ForumException("Article is closed for comments");
            }

            var comment = _mapper.Map<Comment>(commentDTO);
            comment.Author = user;

            _unitOfWork.CommentRepository.Add(comment);
            await _unitOfWork.SaveAsync();

            return _mapper.Map<CommentWithDetailsDTO>(comment);
        }

        public async Task UpdateAsync(Guid id, CommentUpdateDTO commentDTO)
        {
            var comment = await _unitOfWork.CommentRepository.GetByIdAsync(id);
            if (comment == null)
            {
                throw new NotFoundException();
            }

            _mapper.Map(commentDTO, comment);

            _unitOfWork.CommentRepository.Update(comment);
            await _unitOfWork.SaveAsync();
        }

        public async Task DeleteAsync(Guid id)
        {
            var comment = await _unitOfWork.CommentRepository.GetByIdAsync(id);
            if (comment == null)
            {
                throw new NotFoundException();
            }

            _unitOfWork.CommentRepository.Delete(comment);
            await _unitOfWork.SaveAsync();
        }
    }
}