﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Forum.BL.Models;
using Forum.BL.Exceptions;

namespace Forum.BL.Interfaces
{
    /// <summary>
    /// Service for article comments
    /// </summary>
    public interface ICommentService
    {
        /// <summary>
        /// Gets a specific comment by id
        /// </summary>
        /// <param name="id">Guid of the comment to be retrieved</param>
        /// <returns>The comment mapped into <see cref="CommentWithDetailsDTO"/></returns>
        /// <exception cref="NotFoundException">
        /// Thrown when the comment with specified <paramref name="id"/> does not exist
        /// </exception>
        public Task<CommentWithDetailsDTO> GetByIdAsync(Guid id);

        /// <summary>
        /// Gets all comments of the article specified by <paramref name="articleId"/>
        /// </summary>
        /// <param name="articleId">Guid of the article whose comments are to be retrieved</param>
        /// <returns>The list of comments mapped into <see cref="CommentWithDetailsDTO"/></returns>
        /// <exception cref="NotFoundException">
        /// Thrown when the article specified by <paramref name="articleId"/> does not exist
        /// </exception>
        public Task<IEnumerable<CommentWithDetailsDTO>> GetByArticleAsync(Guid articleId);

        /// <summary>
        /// Creates new comments
        /// </summary>
        /// <param name="commentDTO">comment creation data</param>
        /// <param name="authorId">Author guid of the new comment</param>
        /// <returns>Created comment mapped into <see cref="CommentWithDetailsDTO"/></returns>
        /// <exception cref="NotFoundException">
        /// Thrown when the article specified by <paramref name="commentDTO.articleId"/> does not exist
        /// </exception>
        /// <exception cref="ForumException">
        /// Thrown when:
        /// <list type="bullet">
        /// <item><description>The user who creates the comment does not exist</description></item>
        /// <item><description>article is closed for comments </description></item>
        /// </list>
        /// </exception>
        public Task<CommentWithDetailsDTO> CreateAsync(CommentCreateDTO commentDTO, Guid authorId);

        /// <summary>
        /// Updates the comment
        /// </summary>
        /// <param name="id">Guid of the comment to be updated</param>
        /// <param name="commentDTO">comment update data</param>
        /// <exception cref="NotFoundException">
        /// Thrown when the comment specified by <paramref name="id"/> does not exist
        /// </exception>
        public Task UpdateAsync(Guid id, CommentUpdateDTO commentDTO);

        /// <summary>
        /// Deletes the comment
        /// </summary>
        /// <param name="id">Guid of the comment to be deleted</param>
        /// <exception cref="NotFoundException">
        /// Thrown when the comment with specified <paramref name="id"/> does not exist
        /// </exception>
        public Task DeleteAsync(Guid id);
    }
}