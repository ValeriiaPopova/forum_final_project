﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Forum.BL.Models;
using Forum.BL.Exceptions;

namespace Forum.BL.Interfaces
{
    /// <summary>
    /// Service for articles
    /// </summary>
    public interface IArticleService
    {
        /// <summary>
        /// Gets a specific article by id
        /// </summary>
        /// <param name="id">Guid of the article to be retrieved</param>
        /// <returns>The article mapped into <see cref="CommentWithDetailsDTO"/></returns>
        /// <exception cref="NotFoundException">
        /// Thrown when the article with specified <paramref name="id"/> does not exist
        /// </exception>
        public Task<ArticleWithDetailsDTO> GetByIdAsync(Guid id);

        /// <summary>
        /// Gets all articles
        /// </summary>
        /// <returns>The list of articles mapped into <see cref="ArticleWithDetailsDTO"/></returns>
        public Task<IEnumerable<ArticleWithDetailsDTO>> GetAllAsync();

        /// <summary>
        /// Creates new article
        /// </summary>
        /// <param name="articleDTO">article creation data</param>
        /// <param name="authorId">Author guid of the new article</param>
        /// <returns>Created article mapped into <see cref="ArticleWithDetailsDTO"/></returns>
        /// <exception cref="ForumException">
        /// The user who creates the article does not exist
        /// </exception>
        public Task<ArticleWithDetailsDTO> CreateAsync(ArticleCreateDTO articleDTO, Guid authorId);

        /// <summary>
        /// Updates the article
        /// </summary>
        /// <param name="id">Guid of the article to be updated</param>
        /// <param name="articleDTO">article update data</param>
        /// <exception cref="NotFoundException">
        /// Thrown when the article specified by <paramref name="id"/> does not exist
        /// </exception>
        public Task UpdateAsync(Guid id, ArticleUpdateDTO articleDTO);

        /// <summary>
        /// Deletes the article
        /// </summary>
        /// <param name="id">Guid of the article to be deleted</param>
        /// <exception cref="NotFoundException">
        /// Thrown when the article specified by <paramref name="id"/> does not exist
        /// </exception>
        public Task DeleteAsync(Guid id);

        /// <summary>
        /// Updates the status of the article
        /// </summary>
        /// <param name="id">Guid of the article whose status to be updated</param>
        /// <param name="statusDto">article status update data</param>
        /// <exception cref="NotFoundException">
        /// Thrown when the article specified by <paramref name="id"/> does not exist
        /// </exception>
        public Task UpdateStatusAsync(Guid id, ArticleStatusUpdateDTO statusDto);

        /// <summary>
        /// Gets all article by <paramref name="category" />
        /// </summary>
        /// <param name="category">String of the thread whose posts are to be retrieved</param>
        /// <param name="skip">Skip of the thread whose posts are to be retrieved</param>
        /// // <param name="take">Take of the thread whose posts are to be retrieved</param>
        /// <returns>The list of article mapped into <see cref="ArticleModel"/></returns>
        /// <exception cref="NotFoundException">
        /// Thrown when the thread specified by <paramref name="category"/> does not exist
        /// </exception>
        Task<IEnumerable<ArticleWithDetailsDTO>> GetByCategoryAsync(string category, int skip, int? take);
    }
}