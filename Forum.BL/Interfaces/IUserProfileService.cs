﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Forum.BL.Models;
using Forum.BL.Exceptions;

namespace Forum.BL.Interfaces
{
    /// <summary>
    /// Service for userProfile
    /// </summary>
    public interface IUserProfileService
    {
        /// <summary>
        /// Updates the userProfile
        /// </summary>
        /// <param name="id">Guid of the userProfileDTO to be updated</param>
        /// <param name="userProfileDTO">userProfileDTO update data</param>
        /// <exception cref="NotFoundException">
        /// Thrown when the userProfileDTO specified by <paramref name="id"/> does not exist
        /// </exception>
        public Task UpdateAsync(Guid id, UserProfileUpdateDTO userProfileDTO);

        /// <summary>
        /// Gets a specific userProfileDTO by id
        /// </summary>
        /// <param name="id">Guid of the userProfileDTO to be retrieved</param>
        /// <returns>The userProfileDTO mapped into <see cref="UserProfileDTO"/></returns>
        /// <exception cref="NotFoundException">
        /// Thrown when the userProfileDTO with specified <paramref name="id"/> does not exist
        /// </exception>
        public Task<UserProfileDTO> GetByIdAsync(Guid id);
    }
}
