﻿using AutoMapper;
using Forum.BL.Models;
using Forum.DAL.Data.Entities;

namespace Forum.BL.AutoMapper
{
    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            CreateMap<User, UserProfileDTO>().ReverseMap();
            CreateMap<User, UserProfileUpdateDTO>().ReverseMap();

            CreateMap<User, SignUpDTO>().ReverseMap();

            CreateMap<User, UserDTO>();
            CreateMap<User, UserWithDetailsDTO>();
            
            CreateMap<Article, ArticleWithDetailsDTO>();
            CreateMap<ArticleCreateDTO, Article>();
            CreateMap<ArticleUpdateDTO, Article>();
            CreateMap<ArticleStatusUpdateDTO, Article>();

            CreateMap<Comment, CommentWithDetailsDTO>();
            CreateMap<CommentCreateDTO, Comment>();
            CreateMap<CommentUpdateDTO, Comment>();
        }
    }
}