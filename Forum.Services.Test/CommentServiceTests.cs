using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ExpectedObjects;
using FluentAssertions;
using Forum.BL.Exceptions;
using Forum.BL.Models;
using Forum.BL.Services;
using Forum.DAL.Data.Entities;
using Forum.DAL.Data.Interfaces;
using Microsoft.AspNetCore.Identity;
using Moq;
using Xunit;

namespace Service.Tests
{
    public class CommentServiceTests
    {
        private readonly CommentService _sut;
        private readonly Mock<IUnitOfWork> _unitOfWorkMock = new Mock<IUnitOfWork>();

        private readonly Mock<UserManager<User>> _userManagerMock =
            new Mock<UserManager<User>>(Mock.Of<IUserStore<User>>(), null, null, null, null, null, null, null, null);

        public CommentServiceTests()
        {
            _sut = new CommentService(_unitOfWorkMock.Object, _userManagerMock.Object, UnitTestHelper.CreateMapper());
        }

        [Fact]
        public async Task GetByIdAsync_ShouldReturnComment_WhenCommentExists()
        {
            var comment = CommentListWithAuthors.First();
            _unitOfWorkMock.Setup(u => u.CommentRepository.GetByIdWithDetailsAsync(comment.Id))
                .ReturnsAsync(CommentListWithAuthors.First());
            var expected = CommentWithDetailsDtoList.First();

            var result = await _sut.GetByIdAsync(comment.Id);

            result.Should().BeEquivalentTo(expected);
        }

        [Fact]
        public async Task GetByIdAsync_ShouldFail_WhenCommentDoesNotExist()
        {
            var nonexistentCommentId = Guid.NewGuid();
            _unitOfWorkMock.Setup(u => u.CommentRepository.GetByIdWithDetailsAsync(nonexistentCommentId))
                .ReturnsAsync((Comment) null);

            Func<Task> result = async () => await _sut.GetByIdAsync(nonexistentCommentId);

            await result.Should().ThrowAsync<NotFoundException>();
        }

        [Fact]
        public async Task GetByArticleAsync_ShouldReturnComments_WhenArticleExists()
        {
            var article = TestArticle;
            var expected = CommentWithDetailsDtoList.Where(p => p.ArticleId == article.Id);

            _unitOfWorkMock.Setup(u => u.ArticleRepository.GetByIdAsync(article.Id))
                .ReturnsAsync(TestArticle);
            _unitOfWorkMock.Setup(u => u.CommentRepository.GetArticleCommentsWithDetailsAsync(article.Id))
                .ReturnsAsync((Guid id) => CommentListWithAuthors.Where(p => p.ArticleId == id));

            var result = await _sut.GetByArticleAsync(article.Id);

            result.Should().BeEquivalentTo(expected, o => o.WithStrictOrdering());
        }

        [Fact]
        public async Task GetByArticleAsync_ShouldFail_WhenArticleDoesNotExist()
        {
            var nonexistentArticleId = Guid.NewGuid();
            _unitOfWorkMock.Setup(u => u.ArticleRepository.GetByIdAsync(nonexistentArticleId))
                .ReturnsAsync((Article) null);

            Func<Task> result = async () => await _sut.GetByArticleAsync(nonexistentArticleId);

            await result.Should().ThrowAsync<NotFoundException>();
        }

        [Fact]
        public async Task CreateAsync_ShouldCreateComment()
        {
            var commentDTO = CommentCreateDTO;
            var author = TestUser;
            var expectedToAdd = CreateCommentToBeSaved
                .ToExpectedObject(o => o.Ignore(t => t.Author.ConcurrencyStamp));
            var expected = CreateCommentDTO;

            _unitOfWorkMock.Setup(u => u.ArticleRepository.GetByIdAsync(commentDTO.ArticleId))
                .ReturnsAsync(TestArticle);
            _userManagerMock.Setup(um => um.FindByIdAsync(author.Id.ToString()))
                .ReturnsAsync(TestUser);
            _unitOfWorkMock.Setup(u => u.CommentRepository.Add(It.IsAny<Comment>()));

            var result = await _sut.CreateAsync(commentDTO, author.Id);

            result.Should().BeEquivalentTo(expected);

            _unitOfWorkMock.Verify(uow =>
                uow.CommentRepository.Add(It.Is<Comment>(p => expectedToAdd.Equals(p))), Times.Once);
            _unitOfWorkMock.Verify(uow => uow.SaveAsync(), Times.Once);
        }

        [Fact]
        public async Task CreateAsync_ShouldFail_WhenArticleDoesNotExist()
        {
            var commentDTO = CommentCreateDTO;
            var authorId = Guid.NewGuid();
            _unitOfWorkMock.Setup(u => u.ArticleRepository.GetByIdAsync(commentDTO.ArticleId))
                .ReturnsAsync((Article) null);

            Func<Task> result = async () => await _sut.CreateAsync(commentDTO, authorId);

            await result.Should().ThrowAsync<NotFoundException>();

            _unitOfWorkMock.Verify(uow => uow.CommentRepository.Add(It.IsAny<Comment>()), Times.Never);
            _unitOfWorkMock.Verify(uow => uow.SaveAsync(), Times.Never);
        }

        [Fact]
        public async Task CreateAsync_ShouldFail_WhenAuthorDoesNotExist()
        {
            var commentDTO = CommentCreateDTO;
            var nonexistentAuthorId = Guid.NewGuid();
            _unitOfWorkMock.Setup(u => u.ArticleRepository.GetByIdAsync(commentDTO.ArticleId))
                .ReturnsAsync(TestArticle);
            _userManagerMock.Setup(um => um.FindByIdAsync(nonexistentAuthorId.ToString()))
                .ReturnsAsync((User) null);

            Func<Task> result = async () => await _sut.CreateAsync(commentDTO, nonexistentAuthorId);

            await result.Should().ThrowAsync<ForumException>()
                .WithMessage($"User with id '{nonexistentAuthorId}' does not exist");

            _unitOfWorkMock.Verify(uow => uow.CommentRepository.Add(It.IsAny<Comment>()), Times.Never);
            _unitOfWorkMock.Verify(uow => uow.SaveAsync(), Times.Never);
        }

        [Fact]
        public async Task CreateAsync_ShouldFail_WhenArticleIsClosed()
        {
            var commentDTO = CommentCreateDTO;
            var author = TestUser;
            _unitOfWorkMock.Setup(u => u.ArticleRepository.GetByIdAsync(commentDTO.ArticleId))
                .ReturnsAsync(ClosedArticle);
            _userManagerMock.Setup(um => um.FindByIdAsync(author.Id.ToString()))
                .ReturnsAsync(TestUser);

            Func<Task> result = async () => await _sut.CreateAsync(commentDTO, author.Id);

            await result.Should().ThrowAsync<ForumException>()
                .WithMessage("Article is closed for comments");

            _unitOfWorkMock.Verify(uow => uow.CommentRepository.Add(It.IsAny<Comment>()), Times.Never);
            _unitOfWorkMock.Verify(uow => uow.SaveAsync(), Times.Never);
        }

        [Fact]
        public async Task UpdateAsync_ShouldUpdateComment_WhenCommentExists()
        {
            var comment = TestComment;
            var commentDTO = CommentUpdateDTO;
            var expoctedToUpdate = UpdatedCommentToBeSaved.ToExpectedObject();

            _unitOfWorkMock.Setup(u => u.CommentRepository.GetByIdAsync(comment.Id))
                .ReturnsAsync(TestComment);

            await _sut.UpdateAsync(comment.Id, commentDTO);

            _unitOfWorkMock.Verify(uow =>
                uow.CommentRepository.Update(It.Is<Comment>(p => expoctedToUpdate.Equals(p))), Times.Once);
            _unitOfWorkMock.Verify(uow => uow.SaveAsync(), Times.Once);
        }

        [Fact]
        public async Task UpdateAsync_ShouldFail_WhenCommentDoesNotExist()
        {
            var nonexistentCommentId = Guid.NewGuid();
            var commentDTO = CommentUpdateDTO;
            _unitOfWorkMock.Setup(u => u.CommentRepository.GetByIdAsync(nonexistentCommentId))
                .ReturnsAsync((Comment) null);

            Func<Task> result = async () => await _sut.UpdateAsync(nonexistentCommentId, commentDTO);

            await result.Should().ThrowAsync<NotFoundException>();

            _unitOfWorkMock.Verify(uow => uow.CommentRepository.Update(It.IsAny<Comment>()), Times.Never);
            _unitOfWorkMock.Verify(uow => uow.SaveAsync(), Times.Never);
        }

        [Fact]
        public async Task DeleteAsync_ShouldDeleteComment_WhenCommentExists()
        {
            var comment = TestComment;
            var expectedToDelete = TestComment.ToExpectedObject();

            _unitOfWorkMock.Setup(u => u.CommentRepository.GetByIdAsync(comment.Id))
                .ReturnsAsync(TestComment);

            await _sut.DeleteAsync(comment.Id);

            _unitOfWorkMock.Verify(uow =>
                uow.CommentRepository.Delete(It.Is<Comment>(p => expectedToDelete.Equals(p))), Times.Once);
            _unitOfWorkMock.Verify(uow => uow.SaveAsync(), Times.Once);
        }

        [Fact]
        public async Task DeleteAsync_ShouldFail_WhenCommentDoesNotExist()
        {
            var nonexistentCommentId = Guid.NewGuid();
            _unitOfWorkMock.Setup(u => u.CommentRepository.GetByIdAsync(nonexistentCommentId))
                .ReturnsAsync((Comment) null);

            Func<Task> result = async () => await _sut.DeleteAsync(nonexistentCommentId);

            await result.Should().ThrowAsync<NotFoundException>();

            _unitOfWorkMock.Verify(uow => uow.CommentRepository.Delete(It.IsAny<Comment>()), Times.Never);
            _unitOfWorkMock.Verify(uow => uow.SaveAsync(), Times.Never);
        }

        private static IEnumerable<Comment> CommentListWithAuthors =>
            new List<Comment>
            {
                new Comment
                {
                    Id = new Guid("d4376327-f24d-423e-9226-8f85117fe117"),
                    Content = "Vivamus ante sem, vehicula at euismod eu, luctus eleifend erat.",
                    ArticleId = new Guid("8fa32919-5dd4-4e88-90e8-98c9323bdbb6"),
                    AuthorId = new Guid("2b6f10f7-b177-4a64-85af-de55fff46ea2"),
                    PublishDate = new DateTime(2021, 8, 14, 19, 25, 31),
                    Author = new User
                    {
                        Id = new Guid("2b6f10f7-b177-4a64-85af-de55fff46ea2"),
                        UserName = "test1",
                        Email = "test1@ukr.net",
                        Name = "test1",
                        RegistrationDate = new DateTime(2012, 11, 27, 17, 34, 12)
                    }
                },
                new Comment
                {
                    Id = new Guid("0db39598-3cf9-4e98-9d21-50a2c55cf5a1"),
                    Content = "Nulla eget",
                    ArticleId = new Guid("708fb0d0-94c7-4460-95bf-22f18ee38f29"),
                    AuthorId = new Guid("dd7aeae4-98a1-45a4-8fc1-0a7f499e18bb"),
                    PublishDate = new DateTime(2021, 8, 15, 13, 5, 53),
                    Author = new User
                    {
                        Id = new Guid("dd7aeae4-98a1-45a4-8fc1-0a7f499e18bb"),
                        UserName = "test2",
                        Email = "test2@example.com",
                        Name = "test2",
                        RegistrationDate = new DateTime(2016, 3, 16, 5, 19, 59)
                    }
                },
                new Comment
                {
                    Id = new Guid("074b6e15-965b-4a06-add1-302014c4e589"),
                    Content = "Aenean blandit luctus convallis",
                    ArticleId = new Guid("8fa32919-5dd4-4e88-90e8-98c9323bdbb6"),
                    AuthorId = new Guid("6bc56cad-0687-427a-a836-435d25af8575"),
                    PublishDate = new DateTime(2021, 8, 17, 18, 18, 44),
                    Author = new User
                    {
                        Id = new Guid("6bc56cad-0687-427a-a836-435d25af8575"),
                        UserName = "test3",
                        Email = "test3@example.com",
                        Name = "test3",
                        RegistrationDate = new DateTime(2005, 6, 3, 9, 12, 11)
                    }
                }
            };

        private static IEnumerable<CommentWithDetailsDTO> CommentWithDetailsDtoList =>
            new List<CommentWithDetailsDTO>
            {
                new CommentWithDetailsDTO
                {
                    Id = new Guid("d4376327-f24d-423e-9226-8f85117fe117"),
                    Content = "Vivamus ante sem, vehicula at euismod eu, luctus eleifend erat.",
                    ArticleId = new Guid("8fa32919-5dd4-4e88-90e8-98c9323bdbb6"),
                    PublishDate = new DateTime(2021, 8, 14, 19, 25, 31),
                    Author = new UserDTO
                    {
                        Id = new Guid("2b6f10f7-b177-4a64-85af-de55fff46ea2"),
                        Username = "test1",
                        Name = "test1",
                        RegistrationDate = new DateTime(2012, 11, 27, 17, 34, 12)
                    }
                },
                new CommentWithDetailsDTO
                {
                    Id = new Guid("0db39598-3cf9-4e98-9d21-50a2c55cf5a1"),
                    Content = "Nulla eget",
                    ArticleId = new Guid("708fb0d0-94c7-4460-95bf-22f18ee38f29"),
                    PublishDate = new DateTime(2021, 8, 15, 13, 5, 53),
                    Author = new UserDTO
                    {
                        Id = new Guid("dd7aeae4-98a1-45a4-8fc1-0a7f499e18bb"),
                        Username = "test2",
                        Name = "test2",
                        RegistrationDate = new DateTime(2016, 3, 16, 5, 19, 59)
                    }
                },
                new CommentWithDetailsDTO
                {
                    Id = new Guid("074b6e15-965b-4a06-add1-302014c4e589"),
                    Content = "Aenean blandit luctus convallis",
                    ArticleId = new Guid("8fa32919-5dd4-4e88-90e8-98c9323bdbb6"),
                    PublishDate = new DateTime(2021, 8, 17, 18, 18, 44),
                    Author = new UserDTO
                    {
                        Id = new Guid("6bc56cad-0687-427a-a836-435d25af8575"),
                        Username = "test3",
                        Name = "test3",
                        RegistrationDate = new DateTime(2005, 6, 3, 9, 12, 11)
                    }
                }
            };

        private static User TestUser => new User
        {
            Id = new Guid("2b6f10f7-b177-4a64-85af-de55fff46ea2"),
            UserName = "test1",
            Email = "test1@ukr.net",
            Name = "test1",
            RegistrationDate = new DateTime(2012, 11, 27, 17, 34, 12)
        };

        private static Comment TestComment =>
            new Comment
            {
                Id = new Guid("d4376327-f24d-423e-9226-8f85117fe117"),
                Content = "Vivamus ante sem, vehicula at euismod eu, luctus eleifend erat.",
                ArticleId = new Guid("8fa32919-5dd4-4e88-90e8-98c9323bdbb6"),
                AuthorId = new Guid("2b6f10f7-b177-4a64-85af-de55fff46ea2"),
                PublishDate = new DateTime(2021, 8, 14, 19, 25, 31)
            };

        private static Article TestArticle => new Article
        {
            Id = new Guid("8fa32919-5dd4-4e88-90e8-98c9323bdbb6"),
            Topic = "Test article topic",
            Closed = false,
            CreationDate = new DateTime(2017, 4, 10, 16, 27, 25),
            AuthorId = new Guid("6fbd2ad6-e32e-46fd-80f2-59125afb9700")
        };

        private static Article ClosedArticle => new Article
        {
            Id = new Guid("8fa32919-5dd4-4e88-90e8-98c9323bdbb6"),
            Topic = "Closed article topic",
            Closed = true,
            CreationDate = new DateTime(2015, 11, 22, 11, 54, 18),
            AuthorId = new Guid("6fbd2ad6-e32e-46fd-80f2-59125afb9700")
        };

        private static CommentCreateDTO CommentCreateDTO => new CommentCreateDTO()
        {
            Content = "New comment content",
            ArticleId = new Guid("8fa32919-5dd4-4e88-90e8-98c9323bdbb6")
        };

        private static Comment CreateCommentToBeSaved =>
            new Comment
            {
                Content = "New comment content",
                ArticleId = new Guid("8fa32919-5dd4-4e88-90e8-98c9323bdbb6"),
                Author = new User
                {
                    Id = new Guid("2b6f10f7-b177-4a64-85af-de55fff46ea2"),
                    UserName = "test1",
                    Email = "test1@ukr.net",
                    Name = "test1",
                    RegistrationDate = new DateTime(2012, 11, 27, 17, 34, 12)
                }
            };

        private static CommentWithDetailsDTO CreateCommentDTO =>
            new CommentWithDetailsDTO
            {
                Content = "New comment content",
                ArticleId = new Guid("8fa32919-5dd4-4e88-90e8-98c9323bdbb6"),
                Author = new UserDTO
                {
                    Id = new Guid("2b6f10f7-b177-4a64-85af-de55fff46ea2"),
                    Username = "test1",
                    Name = "test1",
                    RegistrationDate = new DateTime(2012, 11, 27, 17, 34, 12)
                }
            };

        private static CommentUpdateDTO CommentUpdateDTO => new CommentUpdateDTO()
        {
            Content = "Updated comment content"
        };

        private static Comment UpdatedCommentToBeSaved =>
            new Comment
            {
                Id = new Guid("d4376327-f24d-423e-9226-8f85117fe117"),
                Content = "Updated comment content",
                ArticleId = new Guid("8fa32919-5dd4-4e88-90e8-98c9323bdbb6"),
                AuthorId = new Guid("2b6f10f7-b177-4a64-85af-de55fff46ea2"),
                PublishDate = new DateTime(2021, 8, 14, 19, 25, 31)
            };
    }
}