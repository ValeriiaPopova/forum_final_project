﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ExpectedObjects;
using FluentAssertions;
using Forum.BL.Exceptions;
using Forum.BL.Models;
using Forum.BL.Services;
using Forum.DAL.Data.Entities;
using Forum.DAL.Data.Interfaces;
using Microsoft.AspNetCore.Identity;
using Moq;
using Xunit;

namespace Service.Tests
{
    public class ArticleServiceTests
    {
        private readonly ArticleService _sut;
        private readonly Mock<IUnitOfWork> _unitOfWorkMock = new Mock<IUnitOfWork>();

        private readonly Mock<UserManager<User>> _userManagerMock =
            new Mock<UserManager<User>>(Mock.Of<IUserStore<User>>(), null, null, null, null, null, null, null, null);

        public ArticleServiceTests()
        {
            _sut = new ArticleService(_unitOfWorkMock.Object, _userManagerMock.Object, UnitTestHelper.CreateMapper());
        }

        [Fact]
        public async Task GetByIdAsync_ShouldReturnArticle_WhenArticleExists()
        {
            var article = ArticleListWithAuthors.First();
            var expected = ArticleWithDetailsDtoList.First();

            _unitOfWorkMock.Setup(u => u.ArticleRepository.GetByIdWithDetailsAsync(article.Id))
                .ReturnsAsync(ArticleListWithAuthors.First());

            var result = await _sut.GetByIdAsync(article.Id);

            result.Should().BeEquivalentTo(expected);
        }

        [Fact]
        public async Task GetByIdAsync_ShouldFail_WhenArticleDoesNotExist()
        {
            var nonexistentArticleId = Guid.NewGuid();

            _unitOfWorkMock.Setup(u => u.ArticleRepository.GetByIdWithDetailsAsync(nonexistentArticleId))
                .ReturnsAsync((Article) null);

            Func<Task> result = async () => await _sut.GetByIdAsync(nonexistentArticleId);

            await result.Should().ThrowAsync<NotFoundException>();
        }

        [Fact]
        public async Task GetAllAsync_ShouldReturArticles()
        {
            var expected = ArticleWithDetailsDtoList;

            _unitOfWorkMock.Setup(u => u.ArticleRepository.GetAllWithDetailsAsync())
                .ReturnsAsync(ArticleListWithAuthors);

            var result = await _sut.GetAllAsync();

            result.Should().BeEquivalentTo(expected, o => o.WithStrictOrdering());
        }

        [Fact]
        public async Task CreateAsync_ShouldCreateArticle_WhenAuthorExists()
        {
            var articleDTO = ArticleCreateDTO;
            var author = TestUser;
            var expectedToAdd = CreateArticleoBeSaved
                .ToExpectedObject(o => o.Ignore(t => t.Author.ConcurrencyStamp));
            var expected = CreateArticleDTO;

            _userManagerMock.Setup(um => um.FindByIdAsync(author.Id.ToString()))
                .ReturnsAsync(TestUser);
            _unitOfWorkMock.Setup(u => u.ArticleRepository.Add(It.IsAny<Article>()));

            var result = await _sut.CreateAsync(articleDTO, author.Id);

            result.Should().BeEquivalentTo(expected);

            _unitOfWorkMock.Verify(uow =>
                uow.ArticleRepository.Add(It.Is<Article>(t => expectedToAdd.Equals(t))), Times.Once);
            _unitOfWorkMock.Verify(uow => uow.SaveAsync(), Times.Once);
        }

        [Fact]
        public async Task CreateAsync_ShouldFail_WhenAuthorDoesNotExist()
        {
            var articleDTO = ArticleCreateDTO;
            var nonexistentAuthorId = Guid.NewGuid();

            _userManagerMock.Setup(um => um.FindByIdAsync(nonexistentAuthorId.ToString()))
                .ReturnsAsync((User) null);

            Func<Task> result = async () => await _sut.CreateAsync(articleDTO, nonexistentAuthorId);

            await result.Should().ThrowAsync<ForumException>();

            _unitOfWorkMock.Verify(uow => uow.ArticleRepository.Add(It.IsAny<Article>()), Times.Never);
            _unitOfWorkMock.Verify(uow => uow.SaveAsync(), Times.Never);
        }

        [Fact]
        public async Task UpdateAsync_ShouldUpdateArticle_WhenArticleExists()
        {
            var article = TestArticle;
            var articleDTO = ArticleUpdateDTO;
            var expectedToUpdate = UpdateArticleToBeSaved.ToExpectedObject();

            _unitOfWorkMock.Setup(u => u.ArticleRepository.GetByIdAsync(article.Id))
                .ReturnsAsync(TestArticle);

            await _sut.UpdateAsync(article.Id, articleDTO);

            _unitOfWorkMock.Verify(uow =>
                uow.ArticleRepository.Update(It.Is<Article>(t => expectedToUpdate.Equals(t))), Times.Once);
            _unitOfWorkMock.Verify(uow => uow.SaveAsync(), Times.Once);
        }

        [Fact]
        public async Task UpdateAsync_ShouldFail_WhenArticleDoesNotExist()
        {
            var nonexistentArticleId = Guid.NewGuid();
            var articleDTO = ArticleUpdateDTO;

            _unitOfWorkMock.Setup(u => u.ArticleRepository.GetByIdAsync(nonexistentArticleId))
                .ReturnsAsync((Article) null);

            Func<Task> result = async () => await _sut.UpdateAsync(nonexistentArticleId, articleDTO);

            await result.Should().ThrowAsync<NotFoundException>();

            _unitOfWorkMock.Verify(uow => uow.ArticleRepository.Update(It.IsAny<Article>()), Times.Never);
            _unitOfWorkMock.Verify(uow => uow.SaveAsync(), Times.Never);
        }

        [Fact]
        public async Task DeleteAsync_ShouldDeleteArticle_WhenArticleExists()
        {
            var article = TestArticle;
            var expectedToDelete = TestArticle.ToExpectedObject();

            _unitOfWorkMock.Setup(u => u.ArticleRepository.GetByIdAsync(article.Id))
                .ReturnsAsync(TestArticle);

            await _sut.DeleteAsync(article.Id);

            _unitOfWorkMock.Verify(uow =>
                uow.ArticleRepository.Delete(It.Is<Article>(t => expectedToDelete.Equals(t))), Times.Once);
            _unitOfWorkMock.Verify(uow => uow.SaveAsync(), Times.Once);
        }

        [Fact]
        public async Task DeleteAsync_ShouldFail_WhenArticleDoesNotExist()
        {
            var nonexistentArticleId = Guid.NewGuid();

            _unitOfWorkMock.Setup(u => u.ArticleRepository.GetByIdAsync(nonexistentArticleId))
                .ReturnsAsync((Article) null);

            Func<Task> result = async () => await _sut.DeleteAsync(nonexistentArticleId);

            await result.Should().ThrowAsync<NotFoundException>();

            _unitOfWorkMock.Verify(uow => uow.ArticleRepository.Delete(It.IsAny<Article>()), Times.Never);
            _unitOfWorkMock.Verify(uow => uow.SaveAsync(), Times.Never);
        }

        [Fact]
        public async Task UpdateStatusAsync_ShouldUpdateArticleStatus_WhenArticleExists()
        {
            var article = TestArticle;
            var statusDto = ArticleStatusUpdateDTO;
            var expectedToUpdate = ArticleWithUpdatedStatusToBeSaved.ToExpectedObject();

            _unitOfWorkMock.Setup(u => u.ArticleRepository.GetByIdAsync(article.Id))
                .ReturnsAsync(TestArticle);

            await _sut.UpdateStatusAsync(article.Id, statusDto);

            _unitOfWorkMock.Verify(uow =>
                uow.ArticleRepository.Update(It.Is<Article>(t => expectedToUpdate.Equals(t))), Times.Once);
            _unitOfWorkMock.Verify(uow => uow.SaveAsync(), Times.Once);
        }

        [Fact]
        public async Task UpdateStatusAsync_ShouldFail_WhenArticleDoesNotExist()
        {
            var nonExistentArticleId = Guid.NewGuid();
            var statusDto = ArticleStatusUpdateDTO;

            _unitOfWorkMock.Setup(u => u.ArticleRepository.GetByIdAsync(nonExistentArticleId))
                .ReturnsAsync((Article) null);

            Func<Task> result = async () => await _sut.UpdateStatusAsync(nonExistentArticleId, statusDto);

            await result.Should().ThrowAsync<NotFoundException>();

            _unitOfWorkMock.Verify(uow => uow.ArticleRepository.Update(It.IsAny<Article>()), Times.Never);
            _unitOfWorkMock.Verify(uow => uow.SaveAsync(), Times.Never);
        }

        private static IEnumerable<Article> ArticleListWithAuthors =>
            new List<Article>
            {
                new Article
                {
                    Id = new Guid("10ceb8e3-b160-4b28-b237-1ecd448a52d3"),
                    Topic = "Test topic 1",
                    Closed = false,
                    CreationDate = new DateTime(2020, 5, 8, 5, 16, 30),
                    AuthorId = new Guid("2b6f10f7-b177-4a64-85af-de55fff46ea2"),
                    Author = new User
                    {
                        Id = new Guid("2b6f10f7-b177-4a64-85af-de55fff46ea2"),
                        UserName = "test1",
                        Email = "test1@ukr.net",
                        Name = "test1",
                        RegistrationDate = new DateTime(2012, 11, 27, 17, 34, 12)
                    }
                },
                new Article
                {
                    Id = new Guid("0a793cc1-0f4f-4766-86e3-2d1f30e03a85"),
                    Topic = "Test topic 2",
                    Closed = false,
                    CreationDate = new DateTime(2020, 5, 3, 5, 12, 51),
                    AuthorId = new Guid("dd7aeae4-98a1-45a4-8fc1-0a7f499e18bb"),
                    Author = new User
                    {
                        Id = new Guid("dd7aeae4-98a1-45a4-8fc1-0a7f499e18bb"),
                        UserName = "test2",
                        Email = "test2@example.com",
                        Name = "test2",
                        RegistrationDate = new DateTime(2016, 3, 16, 5, 19, 59)
                    }
                },
                new Article
                {
                    Id = new Guid("5891e6dc-09ec-4883-9040-80c38c0318ab"),
                    Topic = "Test topic 3",
                    Closed = false,
                    CreationDate = new DateTime(2020, 2, 24, 9, 18, 11),
                    AuthorId = new Guid("6bc56cad-0687-427a-a836-435d25af8575"),
                    Author = new User
                    {
                        Id = new Guid("6bc56cad-0687-427a-a836-435d25af8575"),
                        UserName = "test3",
                        Email = "test3@example.com",
                        Name = "test3",
                        RegistrationDate = new DateTime(2005, 6, 3, 9, 12, 11)
                    }
                },
            };

        private static IEnumerable<ArticleWithDetailsDTO> ArticleWithDetailsDtoList =>
            new List<ArticleWithDetailsDTO>
            {
                new ArticleWithDetailsDTO
                {
                    Id = new Guid("10ceb8e3-b160-4b28-b237-1ecd448a52d3"),
                    Topic = "Test topic 1",
                    Closed = false,
                    CreationDate = new DateTime(2020, 5, 8, 5, 16, 30),
                    Author = new UserDTO
                    {
                        Id = new Guid("2b6f10f7-b177-4a64-85af-de55fff46ea2"),
                        Username = "test1",
                        Name = "test1",
                        RegistrationDate = new DateTime(2012, 11, 27, 17, 34, 12)
                    }
                },
                new ArticleWithDetailsDTO
                {
                    Id = new Guid("0a793cc1-0f4f-4766-86e3-2d1f30e03a85"),
                    Topic = "Test topic 2",
                    Closed = false,
                    CreationDate = new DateTime(2020, 5, 3, 5, 12, 51),
                    Author = new UserDTO
                    {
                        Id = new Guid("dd7aeae4-98a1-45a4-8fc1-0a7f499e18bb"),
                        Username = "test2",
                        Name = "test2",
                        RegistrationDate = new DateTime(2016, 3, 16, 5, 19, 59)
                    }
                },
                new ArticleWithDetailsDTO
                {
                    Id = new Guid("5891e6dc-09ec-4883-9040-80c38c0318ab"),
                    Topic = "Test topic 3",
                    Closed = false,
                    CreationDate = new DateTime(2020, 2, 24, 9, 18, 11),
                    Author = new UserDTO
                    {
                        Id = new Guid("6bc56cad-0687-427a-a836-435d25af8575"),
                        Username = "test3",
                        Name = "test3",
                        RegistrationDate = new DateTime(2005, 6, 3, 9, 12, 11)
                    }
                },
            };

        private static User TestUser => new User
        {
            Id = new Guid("2b6f10f7-b177-4a64-85af-de55fff46ea2"),
            UserName = "test1",
            Email = "test1@ukr.net",
            Name = "test1",
            RegistrationDate = new DateTime(2012, 11, 27, 17, 34, 12)
        };

        private static ArticleCreateDTO ArticleCreateDTO =>
            new ArticleCreateDTO
            {
                Topic = "New article topic"
            };

        private static Article CreateArticleoBeSaved =>
            new Article
            {
                Topic = "New article topic",
                Author = new User
                {
                    Id = new Guid("2b6f10f7-b177-4a64-85af-de55fff46ea2"),
                    UserName = "test1",
                    Email = "test1@ukr.net",
                    Name = "test1",
                    RegistrationDate = new DateTime(2012, 11, 27, 17, 34, 12)
                }
            };

        private static ArticleWithDetailsDTO CreateArticleDTO =>
            new ArticleWithDetailsDTO
            {
                Topic = "New article topic",
                Author = new UserDTO
                {
                    Id = new Guid("2b6f10f7-b177-4a64-85af-de55fff46ea2"),
                    Username = "test1",
                    Name = "test1",
                    RegistrationDate = new DateTime(2012, 11, 27, 17, 34, 12)
                }
            };

        private static Article TestArticle =>
            new Article
            {
                Id = new Guid("10ceb8e3-b160-4b28-b237-1ecd448a52d3"),
                Topic = "Test topic 1",
                Closed = false,
                CreationDate = new DateTime(2020, 5, 8, 5, 16, 30),
                AuthorId = new Guid("2b6f10f7-b177-4a64-85af-de55fff46ea2")
            };

        private static ArticleUpdateDTO ArticleUpdateDTO =>
            new ArticleUpdateDTO
            {
                Topic = "Updated article topic"
            };

        private static Article UpdateArticleToBeSaved =>
            new Article
            {
                Id = new Guid("10ceb8e3-b160-4b28-b237-1ecd448a52d3"),
                Topic = "Updated article topic",
                Closed = false,
                CreationDate = new DateTime(2020, 5, 8, 5, 16, 30),
                AuthorId = new Guid("2b6f10f7-b177-4a64-85af-de55fff46ea2")
            };

        private static ArticleStatusUpdateDTO ArticleStatusUpdateDTO =>
            new ArticleStatusUpdateDTO
            {
                Closed = true
            };

        private static Article ArticleWithUpdatedStatusToBeSaved =>
            new Article
            {
                Id = new Guid("10ceb8e3-b160-4b28-b237-1ecd448a52d3"),
                Topic = "Test topic 1",
                Closed = true,
                CreationDate = new DateTime(2020, 5, 8, 5, 16, 30),
                AuthorId = new Guid("2b6f10f7-b177-4a64-85af-de55fff46ea2")
            };
    }
}